# Nantes Transport

Cette Application consiste à visualiser sur un fond de carte numérique les arrêts de bus de la ville de Nantes et leurs métadata

## Responsable technique du projet
-OUSSAMA (https://gitlab.com/stoon4)

## Responsable fonctionel

-Admir (https://gitlab.com/fouiny-mboz)


## version1.0
-Affichage de la carte
-Focus sur la ville de Nante
-Affichage des arrets de bus
-Affichage des arrets de tram
-Afichage d'une symbolique sur des arrets tram et bus
-Affichage des circuits avec un code couleur différent pour chaque ligne
-Affichage des images par arret

## version2.0
-Améliorations prévues :
-Filtrage des arrets de bus et tram
-Amélioration du style et ajout d'autres images pour les arrets de bus





## Getting started

## Outils techniques

-API OpenStreetMap pour le positionnement
-JavaScript version ES6
-IDE vsCode version 2.0
-Navigateur :Google Chrome, firefox, Edge ...
-Html5
-CSS3
-serveur web apache 2.0
-SonarLint (qualité du code)

## Installation du projet

-se connecter avec ces identifiants dev sur le Boost(GitLab)
-genérer une clé ssh
-Créer un dossier projet sur votre locale et  cloner le liens ssh dans ce dossier
-Ouvrir le projet sur vsCode
-Ouvrir un navigateur et lancer le projet sur navigateur avec la route


## Suggestions for a good README
-Bien suivre les étapes en démarrant les services selon l'ordre  définie


## Contribution
- API externe Data de Nantes (https://data.nantesmetropole.fr)
-Pour le positionnement : https://www.openstreetmap.org

## Critique des données

- Manque des informations concernant:
-Comment différencier les arret de bus et tram
-Comment savoir les correspondances lignes et les liens avec les tram
-Comment associer les lignes et les arrets de bus

## License
-L'utilisation de ce projet est soumis à une license ,toute utilisation fraudileuse et non autorisé est sous peine d'ammende
